//
//  Comment.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 22/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import Argo
import Runes
import SQLite

public struct Comment: Decodable, Cacheable {
    let id: String
    var mediaId: String? = nil
    let createdTime: String
    let text: String
    let from: User?

    public static func create(id: String)(ts: String)(text: String)(from: User) -> Comment {
        return Comment(id: id, mediaId: nil, createdTime: ts, text: text, from: from)
    }

    public static func decode(json: JSON) -> Decoded<Comment> {
        return Comment.create
            <^> json <| "id"
            <*> json <| "created_time"
            <*> json <| "text"
            <*> json <| "from"
    }

    public mutating func setMediaId(mid: String) -> Void {
        self.mediaId = mid
    }

    // MARK: - Database Stuff

    static let idField = Expression<String>("id")
    static let createdTimeField = Expression<String>("created_time")
    static let textField = Expression<String>("text")
    static let mediaIdField = Expression<String?>("media_id")
    static let userIdField = Expression<String?>("user_id")

    public static func fromRow(row: Row) -> Comment {
        return Comment(
            id: row[Comment.idField],
            mediaId: row[Comment.mediaIdField],
            createdTime: row[Comment.createdTimeField],
            text: row[Comment.textField],
            from: User.findWith(row[Comment.userIdField])
        )
    }

    public static func findByMedia(id: String) -> [Comment] {
        var comments: [Comment] = []
        for row in Comment.table().filter(Comment.table()[Comment.mediaIdField] == id) {
            let comment = Comment.fromRow(row)
            comments.append(comment)
        }
        return comments
    }

    public static func table() -> Query {
        return db["comments"]
    }

    public static func initSchema() -> Bool {
        return db.create(table: Comment.table(), ifNotExists: true) { t in
            t.column(Comment.idField, primaryKey: true)
            t.column(Comment.createdTimeField)
            t.column(Comment.textField)
            t.column(Comment.mediaIdField)
            t.column(Comment.userIdField)
        }.failed
    }

    public func save() -> Bool {
        let params = [
            Comment.idField <- self.id,
            Comment.createdTimeField <- self.createdTime,
            Comment.userIdField <- self.from?.id,
            Comment.textField <- self.text,
            Comment.userIdField <- self.from?.id,
            Comment.mediaIdField <- self.mediaId
        ]
        return Comment.table().insert(params).statement.failed || Comment.table().update(params).statement.failed
    }
}