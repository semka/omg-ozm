//
//  Media.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 22/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import Argo
import Runes
import SQLite

public struct Image: Decodable {
    let url: String

    public static func create(url: String) -> Image {
        return Image(url: url)
    }

    public static func decode(json: JSON) -> Decoded<Image> {
        return Image.create
            <^> json <| "url"
    }
}

public struct Images: Decodable {
    let standardResolution: Image

    public static func create(standard: Image) -> Images {
        return Images(standardResolution: standard)
    }

    public static func decode(json: JSON) -> Decoded<Images> {
        return Images.create
            <^> json <| "standard_resolution"
    }
}

public struct Media: Decodable, Cacheable {
    public let id: String
    let caption: String?
    public let user: User?
    public let images: Images?
    let commentsJson: Envelope?

    public var comments: Array<Comment> {
        if let json = commentsJson {
            return json.data
                .map    { return Comment.decode($0).value }
                .filter { $0 != nil }
                .map    { $0! }
        } else {
            return Comment.findByMedia(id)
        }
    }

    public static func create(id: String)(caption: String?)(user: User?)(images: Images?)(comments: Envelope?) -> Media {
        return Media(id: id, caption: caption, user: user, images: images, commentsJson: comments)
    }

    public static func decode(json: JSON) -> Decoded<Media> {
        return Media.create
            <^> json <| "id"
            <*> json <|? ["caption", "text"]
            <*> json <|? "user"
            <*> json <|? "images"
            <*> json <|? "comments"
    }

    // MARK: - Databse Stuff

    static let idField = Expression<String>("id")
    static let userIdField = Expression<String?>("user_id")
    static let imageUrlField = Expression<String?>("image_url")
    static let captionField = Expression<String?>("caption")

    public static func findByUser(userId: String) -> [Media] {
        var result: [Media] = []
        for row in Media.table().filter(Media.table()[Media.userIdField] == userId) {
            result.append(Media.fromRow(row))
        }
        return result
    }

    public static func fromRow(row: Row) -> Media {
        var images: Images? = nil
        if let imageUrl = row[Media.imageUrlField] {
            let image = Image(url: imageUrl)
            images = Images(standardResolution: image)
        }
        return Media(
            id: row[Media.idField],
            caption: row[Media.captionField],
            user: User.findWith(row[Media.userIdField]),
            images: images,
            commentsJson: nil
        )
    }

    public static func table() -> Query {
        return db["media"]
    }

    public static func initSchema() -> Bool {
        return db.create(table: Media.table(), ifNotExists: true) { t in
            t.column(Media.idField, primaryKey: true)
            t.column(Media.userIdField)
            t.column(Media.imageUrlField)
            t.column(Media.captionField)
        }.failed
    }

    public func save() -> Bool {

        let params = [
            Media.idField <- self.id,
            Media.userIdField <- self.user?.id,
            Media.imageUrlField <- self.images?.standardResolution.url,
            Media.captionField <- self.caption
        ]
        let talks = self.comments
        if talks.count > 0 {
            for talk in talks {
                var t = talk
                t.setMediaId(self.id)
                t.save()
            }
        }
        return Media.table().insert(params).statement.failed || Media.table().update(params).statement.failed
    }
}
