//
//  Cacheable.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 26/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import SQLite

public protocol Cacheable {
    func save() -> Bool
    static func initSchema() -> Bool
    static func table() -> Query
    static func fromRow(row: Row) -> Self
}