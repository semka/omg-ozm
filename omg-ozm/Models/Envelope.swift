//
//  APIResponse.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 22/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import Argo
import Runes

public struct Pagination: Decodable {
    public let nextUrl: String
    public let nextMaxId: String

    public static func create(next: String)(nextMaxId: String) -> Pagination {
        return Pagination(nextUrl: next, nextMaxId: nextMaxId)
    }

    public static func decode(json: JSON) -> Decoded<Pagination> {
        return Pagination.create
            <^> json <| "next_url"
            <*> json <| "next_max_id"
    }
}

public struct Meta: Decodable {
    let code: Int

    public static func create(code: Int) -> Meta {
        return Meta(code: code)
    }

    public static func decode(json: JSON) -> Decoded<Meta> {
        return Meta.create <^> json <| "code"
    }
}

public struct Envelope: Decodable {
    let meta: Meta?
    let pagination: Pagination?
    let data: Array<JSON>

    public static func create(meta: Meta?)(pagination: Pagination?)(data: Array<JSON>) -> Envelope {
        return Envelope(meta: meta, pagination: pagination, data: data)
    }

    public static func decode(json: JSON) -> Decoded<Envelope> {
        return Envelope.create
            <^> json <|? "meta"
            <*> json <|? "pagination"
            <*> json <|| "data"
    }
}