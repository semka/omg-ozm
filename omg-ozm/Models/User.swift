//
//  User.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 22/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import Argo
import Runes
import SQLite

public struct UserCounts: Decodable {
    let media: Int
    let followedBy: Int
    let follows: Int

    public static func create(media: Int)(followedBy: Int)(follows: Int) -> UserCounts {
        return UserCounts(media: media, followedBy: followedBy, follows: follows)
    }

    public static func decode(json: JSON) -> Decoded<UserCounts> {
        return UserCounts.create
            <^> json <| "media"
            <*> json <| "followed_by"
            <*> json <| "follows"
    }
}

public struct User: Decodable, Cacheable {
    public let id: String
    public let username: String
    public let profilePicture: String?
    public let bio: String?
    public let website: String?
    public let fullName: String?
    public let counts: UserCounts?

    public static func create
        (id: String)
        (username: String)
        (userpic: String?)
        (bio: String?)
        (website: String?)
        (fullName: String?)
        (counts: UserCounts?) -> User {
            return User(
                id: id,
                username: username,
                profilePicture: userpic,
                bio: bio,
                website: website,
                fullName: fullName,
                counts: counts
            )
    }

    public static func table() -> Query {
        return db["users"]
    }

    public static func decode(json: JSON) -> Decoded<User> {
        return User.create
            <^> json <| "id"
            <*> json <| "username"
            <*> json <|? "profile_picture"
            <*> json <|? "bio"
            <*> json <|? "website"
            <*> json <|? "full_name"
            <*> json <|? "counts"
    }


    // MARK: - Database Stuff

    static let idField = Expression<String>("id")
    static let usernameField = Expression<String>("username")
    static let userpicField = Expression<String?>("profile_picture")
    static let bioField = Expression<String?>("bio")
    static let websiteField = Expression<String?>("website")
    static let fnameField = Expression<String?>("full_name")

    public static func fromRow(row: Row) -> User {
        return User(
            id: row[User.idField],
            username: row[User.usernameField],
            profilePicture: row[User.userpicField],
            bio: row[User.bioField],
            website: row[User.websiteField],
            fullName: row[User.fnameField],
            counts: nil
        )
    }

    public static func findWith(id: String?) -> User? {
        if let id = id {
            let users = User.table().filter(User.table()[User.idField] == id)
            for row in users {
                return User.fromRow(row)
            }
        }
        return nil
    }

    public static func all() -> [User] {
        var models: [User] = []
        for row in User.table() {
            let user = User.fromRow(row)
            let media = Media.findByUser(user.id)
            if media.count > 0 {
                models.append(user)
            }
        }
        return models
    }

    public static func initSchema() -> Bool {
        return db.create(table: User.table(), ifNotExists: true) { t in
            t.column(User.idField, primaryKey: true)
            t.column(User.usernameField)
            t.column(User.userpicField)
            t.column(User.bioField)
            t.column(User.websiteField)
            t.column(User.fnameField)
        }.failed
    }

    public func save() -> Bool {
        let params = [
            User.idField <- self.id,
            User.usernameField <- self.username,
            User.userpicField <- self.profilePicture,
            User.bioField <- self.bio,
            User.websiteField <- self.website,
            User.fnameField <- self.fullName
        ]
        return User.table().insert(params).statement.failed || User.table().update(params).statement.failed
    }
}