//
//  DatabaseInstance.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 25/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import SQLite

let db = DatabaseInstance.current()
var CurrentInstance: Database?

public struct DatabaseInstance {
    private var storage: Database

    init() {
        var storagePath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as! String
        storagePath += "/cache.sqlite3"
        println(storagePath)
        storage = Database(storagePath, readonly: false)
    }

    public static func current() -> Database {
        if CurrentInstance == nil {
            CurrentInstance = DatabaseInstance().storage
        }
        return CurrentInstance!
    }

    public static func initSchema() -> Void {
        User.initSchema()
        Media.initSchema()
        Comment.initSchema()
    }
}
