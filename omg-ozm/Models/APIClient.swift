//
//  InstagramClient.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 22/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import Argo
import Alamofire
import BrightFutures

public struct PaginatedResult<T> {
    public let pagination: Pagination?
    public let data: Array<T>
}

public struct APIClient {

    private static let clientId = "69c0b176290d44fbb06d4c4d1f68020a"

    public static func usersFromCache() -> PaginatedResult<User> {
        let result = PaginatedResult<User>(
            pagination: nil,
            data: User.all()
        )
        return result
    }

    public static func mediaFromCacheForUser(id: String) -> PaginatedResult<Media> {
        let result = PaginatedResult<Media>(
            pagination: nil,
            data: Media.findByUser(id)
        )
        return result
    }

    public static func usersByName(name: String) -> Future<PaginatedResult<User>, NSError> {
        let promise = Promise<PaginatedResult<User>, NSError>()
        let url = "https://api.instagram.com/v1/users/search"

        Alamofire.request(.GET, url, parameters: ["q": name, "client_id": clientId])
            .responseJSON { _, resp, data, error in
                if let error = error {
                    let result = PaginatedResult<User>(
                        pagination: nil,
                        data: User.all()
                    )
                    if result.data.count > 0 {
                        promise.success(result)
                    } else {
                        promise.failure(error)
                    }
                    return
                }
                if let data: AnyObject = data, let envelope: Envelope = decode(data) {
                    let users: Array<User?> = envelope.data.map { return User.decode($0).value }

                    let result = PaginatedResult<User>(
                        pagination: envelope.pagination,
                        data: users.filter { $0 != nil }.map { $0! }
                    )
                    result.data.map { $0.save() }
                    promise.success(result)
                } else {
                    promise.failure(
                        NSError(
                            domain: "com.whatever.omg-ozm",
                            code: -1,
                            userInfo: ["description": "Ужасно это признавать, но мы получили нечто невразумительное"]))
                }
        }
        return promise.future
    }

    public static func mediaFor(userId: String, beforeMaxId: String?) -> Future<PaginatedResult<Media>, NSError> {
        let promise = Promise<PaginatedResult<Media>, NSError>()
        let url = "https://api.instagram.com/v1/users/\(userId)/media/recent"
        var params = ["client_id": clientId, "count": "10"]
        if let maxId = beforeMaxId {
            params = ["client_id": clientId, "count": "10", "max_id": maxId]
        }

        Alamofire.request(.GET, url, parameters: params)
            .responseJSON { _, resp, data, error in
                if let error = error {
                    let result = self.mediaFromCacheForUser(userId)
                    if result.data.count > 0 {
                        promise.success(result)
                    } else {
                        promise.failure(error)
                    }
                    return
                }
                if let data: AnyObject = data, let apiResponse: Envelope = decode(data) {
                    let media: Array<Media?> = apiResponse.data.map { return Media.decode($0).value }
                    let result = PaginatedResult<Media>(
                        pagination: apiResponse.pagination,
                        data: media.filter { $0 != nil }.map { $0! }
                    )
                    result.data.map { $0.save() }
                    promise.success(result)
                } else {
                    promise.failure(
                        NSError(
                            domain: "com.whatever.omg-ozm",
                            code: -1,
                            userInfo: ["description": "Ужасно это признавать, но мы получили нечто невразумительное"]))
                }
            }

        return promise.future
    }
}
