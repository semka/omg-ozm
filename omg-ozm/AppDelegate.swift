//
//  AppDelegate.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 22/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import UIKit
import BrightFutures
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigation: UINavigationController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {


        navigation = UINavigationController(rootViewController: StartController())

        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.backgroundColor = UIColor.whiteColor()
        self.window?.rootViewController = navigation
        self.window?.makeKeyAndVisible()
        setupAppearance()
        DatabaseInstance.initSchema()
        Fabric.with([Crashlytics()])

        return true
    }

    func setupAppearance() -> Void {
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        if let font = UIFont(name: "Chalkduster", size: 22) {
            UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: font]
        }

        if let font = UIFont(name: "Chalkduster", size: 17) {
            UIBarButtonItem.appearance().setTitleTextAttributes(
                [NSFontAttributeName: font],
                forState: UIControlState.Normal
            )
        }
    }

    func applicationWillResignActive(application: UIApplication) {
    }

    func applicationDidEnterBackground(application: UIApplication) {
    }

    func applicationWillEnterForeground(application: UIApplication) {
    }

    func applicationDidBecomeActive(application: UIApplication) {
    }

    func applicationWillTerminate(application: UIApplication) {
    }
}

