//
//  CommentCell.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 25/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var userpic: UIImageView!
    @IBOutlet weak var commentText: UILabel!

    func populateWith(comment: Comment) -> Void {
        if let userpicUrl = comment.from?.profilePicture {
            self.userpic.sd_setImageWithURL(NSURL(string: userpicUrl))
        }
        self.commentText.text = comment.text
    }
}