//
//  MediaItemController.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 25/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit

class MediaItemController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var media: Media
    @IBOutlet weak var tableView: UITableView!
    var showAllComments: Bool = false
    
    // MARK: - Item

    init(media: Media) {
        self.media = media
        super.init(nibName: "MediaItemController", bundle: nil)
        self.title = "OMG, Photo!"
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(
            UINib(nibName: "MediaCell", bundle: nil),
            forCellReuseIdentifier: "mediaCell"
        )
        tableView.registerNib(
            UINib(nibName: "CommentCell", bundle: nil),
            forCellReuseIdentifier: "commentCell"
        )
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    // MARK: - Table Stuff

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: MediaCell = tableView.dequeueReusableCellWithIdentifier("mediaCell") as! MediaCell
            cell.populateWith(media)
            cell.selectionStyle = .None
            return cell
        case 1:
            let cell: CommentCell = tableView.dequeueReusableCellWithIdentifier("commentCell") as! CommentCell
            cell.populateWith(media.comments[indexPath.row])
            cell.selectionStyle = .None
            return cell
        case 2:
            let showAll = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "showAll")
            showAll.textLabel?.text = "Показать все"
            return showAll
        case _: return UITableViewCell()
        }
    }


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        switch indexPath.section {
        case 2:
            self.showAllComments = true
            tableView.reloadData()
        case _: return
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            /* В постановке задачи стоит отображать первые 5 комментариев, так что вот */
            return media.comments.count > 5 ? 5 : media.comments.count
        case 2:
            return showAllComments == false ? 1 : 0
        case _:
            return 0
        }
    }
}