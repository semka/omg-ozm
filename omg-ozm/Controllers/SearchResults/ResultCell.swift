//
//  ResultCell.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 23/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit
import WebImage

class ResultCell: UITableViewCell {
    @IBOutlet weak var userpic: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var bio: UILabel!

    func populateWith(user: User) -> Void {
        if let userpicUrl = user.profilePicture {
            self.userpic.sd_setImageWithURL(
                NSURL(string: userpicUrl),
                placeholderImage: UIImage(named: "userpic")
            )
        }
        self.bio.text = user.fullName
        self.username.text = "@" + user.username
    }
}