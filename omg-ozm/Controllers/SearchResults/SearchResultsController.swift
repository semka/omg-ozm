//
//  SearchResultsController.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 23/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit

class SearchResultsController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var result: PaginatedResult<User>
    @IBOutlet weak var nothingFound: UIView!

    init(result: PaginatedResult<User>) {
        self.result = result
        super.init(nibName: "SearchResultsController", bundle: nil)
        self.title = "OMG, Users!"
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View Stuff

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 120
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.registerNib(
            UINib(nibName: "ResultCell", bundle: nil),
            forCellReuseIdentifier: "resultCell"
        )
        if self.result.data.count == 0 {
            self.nothingFound.hidden = false
        }
    }

    // MARK: - Collection View Stuff

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: ResultCell = tableView.dequeueReusableCellWithIdentifier(
            "resultCell",
            forIndexPath: indexPath
        ) as! ResultCell

        if result.data.count > indexPath.row {
            let user = result.data[indexPath.row]
            cell.populateWith(user)
        }
        return cell
    }


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if result.data.count > indexPath.row {
            let user = result.data[indexPath.row]
            let feedController = FeedController(forUser: user)
            self.navigationController?.pushViewController(feedController, animated: true)
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.result.data.count
    }
}
