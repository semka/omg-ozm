//
//  StartController.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 23/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit
import BrightFutures

class StartController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var offlineAlert: UIView!
    var reachability: Reachability = Reachability.reachabilityForInternetConnection()

    // MARK: - Initialization
    init() {
        self.reachability.startNotifier()
        super.init(nibName: "StartController", bundle: nil)
        self.title = "OMG, Instagram search!"
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.BlackTranslucent
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "reachabilityChanged:",
            name: ReachabilityChangedNotification,
            object: nil
        )
    }

    func reachabilityChanged(sender: Reachability) -> Void {
        self.turnOffline(!Reachability.reachabilityForInternetConnection().isReachable())
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if Reachability.reachabilityForInternetConnection().isReachable() {
            turnOffline(false)
            self.searchField.becomeFirstResponder()
        } else {
            turnOffline(true)
        }
    }

    func turnOffline(offline: Bool) -> Void {
        self.offlineAlert.hidden = !offline
        self.loadingView.hidden = true
        self.searchButton.hidden = offline
        self.searchField.hidden = offline
        if offline {
            self.searchField.resignFirstResponder()
        }
    }

    // MARK: - Actions

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.search(textField)
        return true
    }

    @IBAction func search(sender: AnyObject) {
        if self.searchField.text.isEmpty && self.offlineAlert.hidden {
            UIAlertView(
                title: "Hint",
                message: "Typing something into search field before searching considered good idea, actually",
                delegate: nil,
                cancelButtonTitle: nil,
                otherButtonTitles: "Got it"
            )
            .show()
            return
        }

        self.turnOffline(false)
        self.searchButton.hidden = true
        self.loadingView.hidden = false
        self.searchField.resignFirstResponder()

        APIClient.usersByName(self.searchField.text)
            .onSuccess { users in
                let searchResults = SearchResultsController(result: users)
                self.loadingView.hidden = true
                self.searchButton.hidden = false
                self.navigationController?.pushViewController(searchResults, animated: true)
            }
            .onFailure { error in
                self.loadingView.hidden = true
                self.searchButton.hidden = false
                UIAlertView(
                    title: "Something went wrong",
                    message: "You have no cache, no internet connection and no hope. Please, connect your device to the internet to get some photos",
                    delegate: nil,
                    cancelButtonTitle: nil,
                    otherButtonTitles: "Ok, will do"
                ).show()
            }
    }
}