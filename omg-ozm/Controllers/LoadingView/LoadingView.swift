//
//  LoadingView.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 26/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit

class LoadingView: UIImageView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.image = UIImage(named: "loading")

        var rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Float(M_PI * 2.0)
        rotationAnimation.duration = 1.0
        rotationAnimation.repeatCount = Float.infinity
        self.layer.addAnimation(rotationAnimation, forKey: "rotate")
    }
}