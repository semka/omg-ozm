//
//  MediaCell.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 25/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit
import WebImage

class MediaCell: UITableViewCell {

    @IBOutlet weak var mediaImage: UIImageView!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var caption: UILabel!

    func populateWith(media: Media) -> Void {
        self.caption.hidden = media.caption == nil
        self.caption.text = media.caption
        mediaImage.sd_setImageWithURL(
            NSURL(string: media.images!.standardResolution.url),
            placeholderImage: UIImage(named: "placeholder"),
            options: nil,
            progress: { prog in
                let percents = Float(prog.0) / (Float(prog.1))
                self.progress.hidden = false
                self.progress.progress = percents
            },
            completed: { _ in self.progress.hidden = true }
        )
    }
}