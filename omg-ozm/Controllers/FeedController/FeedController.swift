//
//  FeedController.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 25/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import UIKit

class FeedController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var user: User
    var pagiantion: Pagination?
    var media: [Media] = []
    var hasError: Bool = false
    var canLoadMore: Bool = true

    // MARK: - initialization

    init(forUser: User) {
        user = forUser
        super.init(nibName: "FeedController", bundle: nil)
        self.title = "OMG, User's Feed!"
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(
            UINib(nibName: "ResultCell", bundle: nil),
            forCellReuseIdentifier: "resultCell"
        )
        tableView.registerNib(
            UINib(nibName: "MediaCell", bundle: nil),
            forCellReuseIdentifier: "mediaCell"
        )
        tableView.registerNib(
            UINib(nibName: "LoadingCell", bundle: nil),
            forCellReuseIdentifier: "loadingCell"
        )
        tableView.registerNib(
            UINib(nibName: "SeparatorCell", bundle: nil),
            forCellReuseIdentifier: "separatorCell"
        )
        tableView.registerNib(
            UINib(nibName: "PrivateTimeline", bundle: nil),
            forCellReuseIdentifier: "errorCell"
        )
    }

    // MARK: - Table View Stuff

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return self.profileCell(user)
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("separatorCell") as! UITableViewCell
            cell.selectionStyle = .None
            return cell
        case 2:
            if self.hasError {
                let cell = tableView.dequeueReusableCellWithIdentifier("errorCell") as! UITableViewCell
                cell.selectionStyle = .None
                return cell
            } else {
                return self.mediaCell(media[indexPath.row])
            }
        case 3:
            return tableView.dequeueReusableCellWithIdentifier("loadingCell") as! UITableViewCell
        default:
            return UITableViewCell()
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 132
        case 1:
            return 60
        case 2:
            return 300
        case _:
            return 42
        }

    }

    func mediaCell(media: Media) -> MediaCell {
        let cell: MediaCell = tableView.dequeueReusableCellWithIdentifier("mediaCell") as! MediaCell
        cell.populateWith(media)
        return cell
    }

    func profileCell(user: User) -> ResultCell {
        let cell: ResultCell = tableView.dequeueReusableCellWithIdentifier("resultCell") as! ResultCell
        cell.selectionStyle = .None
        cell.populateWith(user)
        return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            if self.hasError {
                return 1
            } else {
                return media.count
            }
        case 3:
            if self.hasError || !self.canLoadMore {
                return 0
            }
            return 1
        default:
            return 0
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }

    func tableView(
        tableView: UITableView,
        willDisplayCell cell: UITableViewCell,
        forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 3 {
            self.loadNext(pagiantion?.nextMaxId)
        }
    }


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        if indexPath.section == 2 && !self.hasError {
            let mediaCtrl = MediaItemController(media: media[indexPath.row])
            self.navigationController?.pushViewController(mediaCtrl, animated: true)
        }
    }

    // MARK: - Data Loading

    func loadNext(before: String?) -> Void {
        if self.media.count > 0 && before == nil {
            hasError = false
            canLoadMore = false
            self.tableView.reloadData()
            return
        }
        APIClient.mediaFor(user.id, beforeMaxId: before)
            .onSuccess { mediaResult in
                self.hasError = false
                let oldCount = self.media.count
                self.media.extend(mediaResult.data)
                self.pagiantion = mediaResult.pagination

                let addedCount = self.media.count
                var newIps: [NSIndexPath] = []
                if addedCount == 0 {
                    self.canLoadMore = false
                    self.tableView.reloadData()
                    return
                }
                for index in oldCount ... addedCount - 1 {
                    newIps.append(NSIndexPath(forRow: index, inSection: 2))
                }
                self.tableView.insertRowsAtIndexPaths(
                    newIps,
                    withRowAnimation: UITableViewRowAnimation.Fade
                )
            }
            .onFailure { error in
                if self.media.count == 0 {
                    self.hasError = true
                    self.tableView.reloadData()
                }
            }
    }
}