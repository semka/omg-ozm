//
//  APIClientSpec.swift
//  omg-ozm
//
//  Created by Semyon Novikov on 22/07/15.
//  Copyright (c) 2015 whatever. All rights reserved.
//

import Foundation
import Quick
import Nimble
import OmgOmz
import BrightFutures
import Result

class APIClientSpec: QuickSpec {

    override func spec() {

        describe("API Client") {

            it("should be able to search users by name") {
                var result: PaginatedResult<User>?
                APIClient.usersByName("stephenfryactually").onSuccess { res in
                    result = res
                }
                expect(result).toEventuallyNot(beNil(), timeout: 10)
                expect(result?.data.count).toEventually(beGreaterThan(0), timeout: 10)
            }

            it("should be able to fetch media for user id") {
                var result: PaginatedResult<Media>?
                APIClient.usersByName("stephenfryactually").onSuccess { users in
                    return users
                }.andThen { users in
                    switch users {
                    case .Success(let x):
                        if let userId = x.value.data.first?.id {
                            APIClient.mediaFor(userId, beforeMaxId: nil).onSuccess { media in
                                result = media
                            }
                        }
                    case .Failure(_): break;
                    }
                }
                expect(result).toEventuallyNot(beNil(), timeout: 10)
                expect(result?.data.count).toEventually(beGreaterThan(1), timeout: 10)
            }

            it("should be able to load next 'page' of user's media by paginated result") {
                var firstPage: PaginatedResult<Media>?
                var result: PaginatedResult<Media>?
                APIClient.usersByName("stephenfryactually").onSuccess { users in
                    return users
                }.andThen { users in
                    switch users {
                    case .Success(let x):
                        if let userId = x.value.data.first?.id {
                            APIClient.mediaFor(userId, beforeMaxId: nil).onSuccess { media in
                                return media
                            }
                            .andThen { prev in
                                firstPage = prev.value
                                APIClient.mediaFor(userId, beforeMaxId: prev.value!.pagination!.nextMaxId)
                                    .onSuccess { next in
                                        result = next
                                    }
                            }
                        }
                    case .Failure(_): break;
                    }
                }

                expect(result).toEventuallyNot(beNil(), timeout: 10)
                expect(firstPage).toEventuallyNot(beNil(), timeout: 10)
                expect(result?.data.first?.id != firstPage?.data.first?.id).toEventually(beTruthy(), timeout: 10)
                expect(result?.data.count).toEventually(beGreaterThan(1), timeout: 10)
            }
        }
    }
}